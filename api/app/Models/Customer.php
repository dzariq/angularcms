<?php namespace App\Models;


class Customer extends BaseModel
{
    use TimestampsFormatTrait;

    protected $table = 'customer';
    protected $fillable = ['lastname', 'firstname', 'email', 'phone', 'address', 'sex', 'active'];
    public $timestamps = true;
}
