1. update src/app/index.js -> var admin_path = "{api path}"
2. command -> npm install && bower install or bower install --allow-root on root linux
(allow port on centos)
$ sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
$ sudo firewall-cmd --reload

3. gulp 
4. access {api path}/backend
4. login => admin@rakanoth.com/password

Nginx

    location ~ ^/backend {
        rewrite ^/backend(.*) /assets-backend/$1 break;
    }
Apache

    RewriteRule ^backend/(.*)\.([^\.]+)$ /assets-backend/$1.$2 [L]